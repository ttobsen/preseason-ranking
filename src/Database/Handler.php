<?php

namespace Ttobsen\PreseasonRanking\Database;

const DB_FILE = '../web/database.db';

class Handler
{

    private $db;

    function __construct($db_file = DB_FILE)
    {
        $this->db = new \SQLite3($db_file);
    }

    public function getUsers()
    {

        $sql = "SELECT * FROM users WHERE name IS NOT NULL";

        $res = $this->db->query($sql);
        $users = array();
        while ($row = $res->fetchArray()) {
            $users[$row['id']] = $row;
        }
        return $users;
    }

    public function getTrainings()
    {

        $sql = "SELECT * FROM trainings ORDER BY date DESC";

        $res = $this->db->query($sql);
        $trainings = array();
        while ($row = $res->fetchArray()) {
            $trainings[] = $row;
        }
        return $trainings;
    }

    public function getExtraTrainings()
    {

        $sql = "SELECT * FROM extra_trainings ORDER BY date DESC";

        $res = $this->db->query($sql);
        $trainings = array();
        while ($row = $res->fetchArray()) {
            $trainings[] = $row;
        }
        return $trainings;
    }

    public function createTraining($points, $neg_points, $users, $date, $note)
    {
        $users_str = implode(',', $users);
        $sql = "INSERT INTO trainings(points, users, date, note, neg_points)
                VALUES($points, '$users_str', $date, '$note', $neg_points)";
        $this->db->query($sql);
    }

    public function createExtraTraining($users, $points, $team_points, $date, $note)
    {
        foreach ($users as $user) {
            $sql = "INSERT INTO extra_trainings(user, points, date, note, team_points) VALUES($user, $points, $date, '$note', $team_points)";
            $this->db->query($sql);
        }
    }

    public function clearTable($db_name)
    {
        $this->db->query("DELETE FROM $db_name");
        $this->db->query("VACUUM");
    }

}