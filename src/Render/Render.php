<?php

namespace Ttobsen\PreseasonRanking\Render;

use Ttobsen\PreseasonRanking\Database\Handler;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


const DEFAULT_CONFIG = array(
    "refresh_route" => "/"
);


class Render
{

    public static function routeDefault()
    {
        $db              = new Handler();
        $user_data       = $db->getUsers();
        $trainings       = $db->getTrainings();
        $extra_trainings = $db->getExtraTrainings();

        foreach ($user_data as $user) {
            $user_id = $user['id'];
            $user_points = Points::calcPoints($user_id, $trainings, $extra_trainings);
            $user_data[$user_id]['points']          = $user_points["total"];
            $user_data[$user_id]['training_points'] = $user_points["total"] - $user_points["extra"];
            $user_data[$user_id]['extra_points']    = $user_points["extra"];
            $user_data[$user_id]['week_extra']      = Points::calcPointsExtraTraining($user_id, $extra_trainings);
            $user_data[$user_id]['week_team']       = Points::calcPointsExtraTeam($user_id, $extra_trainings);
        }

        $ranking_data = Points::createRankings($user_data);

        $config                        = DEFAULT_CONFIG;
        $config["users"]               = $ranking_data;
        $config["made_points"]         = Points::getTotalPoints($user_data);
        $config["negative_points"]     = Points::getTotalNegativePoints($trainings);
        $config["total_points"]        = $config["made_points"] - $config["negative_points"];
        $config["average_points"]      = round($config["total_points"] / (count($user_data) - 1));
        $config['MilestoneTable']      = Points::getMilestoneTable();
        $config['MilestoneGraph']      = Points::createMilestoneGraph($config["total_points"]);
        $config['MilestoneTrendGraph'] = Points::createMilestoneTrendGraph($trainings, $extra_trainings, $user_data);

        $twig = self::createTwig();
        echo $twig->render('index.html.twig', $config);

    }

    public static function routeInfo()
    {
        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/info";

        $twig = self::createTwig();
        echo $twig->render('info.html.twig', $config);
    }

    public static function routeTrainings()
    {
        $db              = new Handler();
        $user_data       = $db->getUsers();
        $trainings       = $db->getTrainings();
        $extra_trainings = $db->getExtraTrainings();

        foreach ($trainings as $key => $training) {
            $attendees_ids = explode(",", $training['users']);
            $attendees = array();
            foreach ($attendees_ids as $id)
                $attendees[] = $user_data[$id]['name'];
            $trainings[$key]['attendees'] = implode(', ', $attendees);
        }

        foreach ($extra_trainings as $key => $training)
            $extra_trainings[$key]['attendee'] = $user_data[$training['user']]['name'];

        $config                    = DEFAULT_CONFIG;
        $config['refresh_route']   = "/trainings";
        $config['trainings']       = $trainings;
        $config['extra_trainings'] = $extra_trainings;
        $config['users']           = $user_data;

        $twig = self::createTwig();
        echo $twig->render('trainings.html.twig', $config);
    }

    public static function routeAdmin()
    {
        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/admin";

        $twig = self::createTwig();
        echo $twig->render('admin.html.twig', $config);
    }

    public static function routeAddTraining()
    {

        $db        = new Handler();
        $user_data = $db->getUsers();

        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/admin";
        $config['users']         = $user_data;
        $config['date']          = self::createDateStr();

        $twig = self::createTwig();
        echo $twig->render('addtraining.html.twig', $config);
    }

    public static function routeCreateTraining()
    {
        $users = array();
        for ($i = 0; $i < 50; $i++) {
            if (isset($_POST[$i])) {
                $users[] = $i;
            }
        }

        $points     = $_POST['points'];
        $neg_points = abs($_POST['neg_points']);
        $date       = strtotime($_POST['date']);
        $note       = $_POST['notes'];

        $db = new Handler();
        $db->createTraining($points, $neg_points, $users, $date, $note);
        unset($db);

        self::routeAdmin();
    }

    public static function routeAddExtraTraining()
    {

        $db        = new Handler();
        $user_data = $db->getUsers();

        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/admin";
        $config['users']         = $user_data;
        $config['date']          = self::createDateStr();

        $twig = self::createTwig();
        echo $twig->render('addextratraining.html.twig', $config);
    }

    public static function routeCreateExtraTraining()
    {

        $users = array();
        for ($i = 0; $i < 50; $i++) {
            if (isset($_POST[$i])) {
                $users[] = $i;
            }
        }

        $points = $_POST['points'];
        $team_points = Points::calcExtraTrainingTeamPoints(count($users), $points);
        $date = strtotime($_POST['date']);
        $note = $_POST['notes'];

        $db = new Handler();
        $db->createExtraTraining($users, $points, $team_points, $date, $note);
        unset($db);

        self::routeAdmin();
    }

    private static function createTwig()
    {
        $loader = new FilesystemLoader('../template');
        $twig = new Environment($loader);
        $twig->enableDebug();
        return $twig;
    }

    private static function createDateStr()
    {
        return date("Y-m-d")." 18:30";
    }

}
