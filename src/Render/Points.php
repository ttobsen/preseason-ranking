<?php

namespace Ttobsen\PreseasonRanking\Render;

use Amenadiel\JpGraph\Graph;
use Amenadiel\JpGraph\Plot;
use Amenadiel\JpGraph\Text;


const START_DATE = "04.06.2021";
const MILESTONES = [
    ["points" =>  50000, "deadline" => "21.07.2021 23:59", "done" =>  51236, "notice" => "Schwimmbad Spaß 31.7."],
    ["points" =>  75000, "deadline" => "11.08.2021 23:59", "done" =>  82170, "notice" => "Sportspaß WE 13.8. - 15.8."],
    ["points" => 100000, "deadline" => "31.08.2021 23:59", "done" => 110795, "notice" => "Buechereck Saison Warmup"],
    ["points" => 250000, "deadline" => "31.12.2021 23:59", "done" =>  false, "notice" => "Irgendwas verruecktes zum Saisonabschluß"]];


class Points
{
    public static function calcPoints($user_id, $trainings, $extra_trainings)
    {

        $points       = 0;
        $extra_points = 0;

        foreach ($trainings as $training) {
            $users = explode(",", $training['users']);
            if (in_array($user_id, $users)) {
                $points += $training['points'];
            }
        }

        foreach ($extra_trainings as $training)
            if ($user_id == $training['user']) {
                $extra_points += $training['points'];
                $extra_points += $training['team_points'];
            }

        return array("total" => $points+$extra_points, "extra" => $extra_points);

    }

    public static function calcPointsExtraTraining($user_id, $extra_trainings)
    {
        $points = 0;
        $deadline = strtotime("last friday 22:00");

        foreach ($extra_trainings as $training)
            if ($training['user'] == $user_id && $training['date'] >= $deadline)
                $points += $training['points'];

        return $points;
    }

    public static function calcPointsExtraTeam($user_id, $extra_trainings)
    {
        $points = 0;
        $deadline = strtotime("last friday 22:00");

        foreach ($extra_trainings as $training)
            if ($training['user'] == $user_id && $training['date'] >= $deadline)
                $points += $training['team_points'];

        return $points;
    }

    public static function calcExtraTrainingTeamPoints($num_users, $extra_training_points)
    {
        $factor =
            ($num_users == 0) ? 0.0 :
            (($num_users >= 6) ? 0.5 : 0.1 * ($num_users - 1));
        return round($factor * $extra_training_points);
    }

    public static function calcPointsTrend($trainings, $extra_trainings, $user_data)
    {
        $trend = [0, ];

        $active_users = array_column($user_data, "id");

        $NUM_DAYS = self::calcDateDiff(START_DATE, date("d.m.Y 23:59"));
        for ($i = 1; $i <= $NUM_DAYS; $i++) {
            $points = 0;
            foreach ($trainings as $training) {
                $date_diff = ($training["date"] - strtotime(START_DATE)) / (24 * 3600);
                if ($date_diff < $i) {
                    $user_ids = array_intersect(
                        explode(",", $training['users']),
                        $active_users);
                    $num_users = count($user_ids);
                    $points += $num_users * $training['points'];
                    $points -= abs($training['neg_points']);
                }
            }
            foreach ($extra_trainings as $training) {
                $date_diff = ($training["date"] - strtotime(START_DATE)) / (24 * 3600);
                if ($date_diff < $i) {
                    if (in_array($training['user'], $active_users)) {
                        $points += $training['points'];
                        $points += $training['team_points'];
                    }
                }
            }
            $trend[] = $points;
        }

        return $trend;

    }

    public static function calcTrendOutlook($trend_data, $delta_t)
    {
        $x2 = count($trend_data)-1;
        $y2 = $trend_data[$x2];
        $x1 = $x2 - $delta_t;
        $y1 = $trend_data[$x1];

        $m = ($y2 - $y1) / ($x2 - $x1);
        $a = ($x2*$y1 - $x1*$y2) / ($x2 - $x1);

        $y0 = 0;
        $x0 = ($y0 - $a) / $m;

        $yu = 250000;
        $xu = ($yu - $a) / $m;

        return [[$y0, $yu], [$x0, $xu]];
    }

    public static function getTotalPoints($user_data)
    {
        $total_points = 0;

        foreach ($user_data as $user)
            $total_points += $user['points'];

        return $total_points;
    }

    public static function getTotalNegativePoints($trainings)
    {
        $total_points = 0;

        foreach ($trainings as $training)
            $total_points += abs($training["neg_points"]);

        return $total_points;
    }

    public static function createRankings($user_data)
    {
        $points  = array_column($user_data, 'points');
        $sorted_data = $user_data;
        array_multisort($points, SORT_DESC, $sorted_data);

        foreach(array_keys($sorted_data) as $key)
            $sorted_data[$key]['rank'] = 1;

        for ($i=1; $i < count($sorted_data); $i++) {
            $prev_rank = $sorted_data[$i-1]['rank'];
            $sorted_data[$i]['rank'] =
                ($sorted_data[$i]['points'] < $sorted_data[$i-1]['points']) ?
                    $prev_rank + 1 : $prev_rank;
        }

        return $sorted_data;
    }

    public static function getMilestoneTable()
    {
        return MILESTONES;
    }

    public static function createMilestoneGraph($total_points)
    {
        // Create the Pie Graph.
        $current_points   = array();
        $remaining_points = array();
        foreach (MILESTONES as $key => $val) {
            if ($val["done"] != false) {
                $current_points[$key] = $val["done"];
                $remaining_points[$key] = 0;
            } else {
                if ($total_points < $val) {
                    $current_points[$key]   = $total_points;
                    $remaining_points[$key] = $val["points"] - $total_points;
                } else {
                    $current_points[$key]   = $val["points"];
                    $remaining_points[$key] = 0;
                }
            }
        }

        // Create the graph. These two calls are always required
        $graph = new Graph\Graph(800,300);
        $graph->SetScale("textlin");

        $graph->Set90AndMargin(120,20,50,30);
        $graph->SetShadow();

        // Create the bar plots
        $b1plot = new Plot\BarPlot($current_points);
        $b1plot->SetFillColor("green");

        $b2plot = new Plot\BarPlot($remaining_points);
        $b2plot->SetFillColor("red");

        // Create the grouped bar plot
        $gbplot = new Plot\AccBarPlot(array($b1plot,$b2plot));

        // ...and add it to the graPH
        $graph->Add($gbplot);

        $graph->title->SetFont(FF_FONT1,FS_BOLD);
        $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
        $graph->yaxis->SetMajTickPositions(range(0, 500000, 25000));
        $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
        $xlabels = array(
            "Beginner", "Spannend", "Saustark", "Elitär", "Damen 1 und 2\nmüssen tauschen", "Der Coach bereut\ndie Challenge"
        );
        $graph->xaxis->SetTickLabels($xlabels);

        foreach (MILESTONES as $key => $val) {
            $percentage = ($total_points >= $val["points"]) ? 100.0 : 100 * $current_points[$key] / $val["points"];
            $txt = new Text\Text(number_format($percentage, 2, '.', '') . "%");
            $txt->SetAlign('left');
            $txt->SetScalePos($key+0.4, $total_points+1000);
            $graph->AddText($txt);
        }

        $graph->Stroke(_IMG_HANDLER);
        ob_start();
        $graph->img->Stream();
        $image_data = ob_get_contents();
        //Stop the buffer/clear it.
        ob_end_clean();
        //Set the variable equal to the base 64 encoded value of the stream.
        //This gets passed to the browser and displayed.
        return base64_encode($image_data);
    }

    public static function createMilestoneTrendGraph($trainings, $extra_trainings, $user_data)
    {

        $visible_milestones = array_slice(MILESTONES, 3, 1);
        $ref_milestone = MILESTONES[3];

        // Create the graph. These two calls are always required
        $graph = new Graph\Graph(800,800);
        $graph->SetScale('linlin', 0, 0, 0, self::calcDateDiff(START_DATE, $ref_milestone["deadline"]));
        $graph->SetMargin(50, 50, 50, 50);

        foreach ($visible_milestones as $val) {
            $end_time = self::calcDateDiff(START_DATE, $val["deadline"]);
            $plot = new Plot\LinePlot([0, $val["points"]], [0, $end_time]);
            //$plot->SetWeight(3);
            //$plot->SetStyle("dashed");
            $graph->Add($plot);
            $plot->SetLegend("Milestone ".strval($val["points"]));
            $plot->SetCenter();
        }

        $trend_data = self::calcPointsTrend($trainings, $extra_trainings, $user_data);
        $plot = new Plot\LinePlot($trend_data);
        //$plot->SetWeight(3);
        $plot->SetLegend("Punkteverlauf");
        $graph->Add($plot);

        $outlooks = [14, 28];
        foreach ($outlooks as $outlook_days) {
            $outlook_data = self::calcTrendOutlook($trend_data, $outlook_days);
            $plot = new Plot\LinePlot($outlook_data[0], $outlook_data[1]);
            //$plot->SetWeight(1);
            $plot->SetLegend("$outlook_days Tage Prognose");
            $graph->Add($plot);
        }

        $txt = new Text\Text(strval($trend_data[count($trend_data)-1]));
        $txt->SetAlign('center');
        $txt->SetScalePos(count($trend_data)-1, $trend_data[count($trend_data)-1] + 1700);
        $graph->AddText($txt);

        $graph->ygrid->Show();
        $graph->xgrid->Show();

        $graph->Stroke(_IMG_HANDLER);
        ob_start();
        $graph->img->Stream();
        $image_data = ob_get_contents();
        //Stop the buffer/clear it.
        ob_end_clean();
        //Set the variable equal to the base 64 encoded value of the stream.
        //This gets passed to the browser and displayed.
        return base64_encode($image_data);
    }

    public static function calcDateDiff($date1, $date2)
    {
        $diff = strtotime($date2) - strtotime($date1);
        return ceil($diff / (24*60*60));
    }

}