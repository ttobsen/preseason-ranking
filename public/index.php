<?php

require_once __DIR__.'/../vendor/autoload.php';

use Ttobsen\PreseasonRanking\Render\Render;
use Steampixel\Route;

Route::add("/",          function() {Render::routeDefault();});
Route::add("/trainings", function() {Render::routeTrainings();});
Route::add("/info",      function() {Render::routeInfo();});

Route::add("/admin",               function() {Render::routeAdmin();});
Route::add("/addtraining",         function() {Render::routeAddTraining();});
Route::add("/createtraining",      function() {Render::routeCreateTraining();}, 'post');
Route::add("/addextratraining",    function() {Render::routeAddExtraTraining();});
Route::add("/createextratraining", function() {Render::routeCreateExtraTraining();}, 'post');

Route::run('/');

?>
